let table = require('./table');
let deck = require('./deck');

class Player {
	constructor(socket, monsters_count) {
		this.socket = socket;
		this.name = socket.id;
		this.cards = [];
		this.table = new table.Table(monsters_count);
	}

	initGame() {
		this.socket.emit('init_game_user', {
			cards: this.cards
		});
	}

	addCardToMonster(card_id, monster_index, part) {
		if ((this.hasCard(card_id)) && this.table.monsters[monster_index].allowAddCard(card_id, part)) {
			this.table.monsters[monster_index][part] = deck.cards[card_id - 1];
			this.removeCard(card_id);
			return true;
		} else {
			return false;
		}
	}

	hasCard(card_id) {
		for (let i = 0; i < this.cards.length; i++) {
			if (this.cards[i].id == card_id) {
				return true;
			}
		}
		return false;
	}

	removeCard(card_id) {
		for (let i = 0; i < this.cards.length; i++) {
			if (this.cards[i].id == card_id) {
				this.cards.splice(i, 1);
				return;
			}
		}
	}
}

exports.Player = Player;
