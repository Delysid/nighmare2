let player = require('./player');
let card = require('./card');
let deck = require('./deck');

class Game {
	constructor(id, players_count = 2, monsters_count = 5) {
		this.id = id;
		this.players_count = players_count;
		this.monsters_count = monsters_count;
		this.INITIAL_CARDS_COUNT = 15;
		this.players = [];
		this.deck = [];
		this.trash = [];
		this.first_step_legion = null;
		this.game_status = 'prepeareing';
		this.skill_action = {
			active: false
		};
		deck.cards.forEach((deck_card) => {
			this.deck.push(new card.Card(deck_card.id, deck_card.legion, deck_card.is_head, deck_card.is_body, deck_card.is_legs, deck_card.skill));
		});
		this.shuffle(this.deck);
	}

	isPlayersFull() {
		return this.players.length >= this.players_count
	}

	addPlayer(socket) {
		if (!this.isPlayersFull() && this.game_status == 'prepeareing') {
			socket.join(this.id);
			let new_player = new player.Player(socket, this.monsters_count);
			this.players.push(new_player);
			socket.broadcast.emit('join_game', {game_id: this.id, user_name: new_player.name});
			socket.emit('you_join_game', this.id);
		}
	}

	shuffle(arr) {
		arr.sort(() => (Math.random() > 0.5))
	}

	startGame(socket) {
		if (this.isPlayersFull() && this.isUserInGame(socket) && this.game_status == 'prepeareing') {
			this.game_status = 'run';
			this.players.forEach((game_player) => {
				for (let i = 0; i < this.INITIAL_CARDS_COUNT; i++) {
					this.getCard(game_player);
				}
			});
			this.current_player_index = 0;
			this.step = 0;
			io.sockets.in(this.id).emit('start_game', {
				game_id: this.id,
				max_users: this.players_count,
				max_monsters: this.monsters_count,
				current_player_index: this.current_player_index,
				current_step: this.step,
				deck: deck.cards,
				players: this.players.map((game_player, index) => {
					return {
						user_id: game_player.socket.id,
						user_index: index,
						hand_count: game_player.cards.length
					}
				})
			});
			this.players.forEach((game_player) => game_player.initGame());
			return true;
		}
		return false;
	}

	actionGetCard(socket) {
		if (this.game_status == 'run' && this.players[this.current_player_index].socket == socket) {
			let card = this.getCard(this.players[this.current_player_index]);
			socket.broadcast.to(this.id).emit('opponent_add_card', socket.id);
			socket.emit('add_card', {card: card});
			this.nextStep();
			console.log('User: ' + socket.id + ' get card');
		}
	}

	actionPutCard(card_id, monster_id, part, socket) {
		let user = this.getCurrentUser();
		if (this.game_status == 'run' && user.socket == socket) {
			if(this.skill_action.card && this.skill_action.card.skill == SKILLS.SINGER) {
				if(this.skill_action.singer_cards.some((card) => card.id == card_id)
					&& user.table.monsters[monster_id].allowAddCard(card_id, part)) {
					user.table.monsters[monster_id][part] = deck.cards[card_id - 1];
					this.skill_action.singer_cards = this.skill_action.singer_cards.filter((card) => card.id != card_id)
					io.sockets.in(this.id).emit('add_card_to_monster', {
						card_id: card_id,
						monster_index: monster_id,
						part: part,
						user_id: socket.id
					});
					if (user.table.monsters[monster_id].is_full()) {
						socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
						this.activateMonster(user, monster_id);
					} else if (!this.skill_action.singer_cards.length) {
						this.useSkills()
					} else {
						if(this.skill_action.singer_cards.some((card) => user.table.monsters.some((monster) => monster.allowAddCard(card.id, 'legs') || monster.allowAddCard(card.id, 'body') || monster.allowAddCard(card.id, 'head')))) {
							socket.emit('singer_put_card', {
								cards: this.skill_action.singer_cards
							});
							this.skill_action.result = 'wait';
						} else {
							socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
							this.skill_action.singer_cards.forEach((card) => this.trash.push(card));
							this.skill_action.singer_cards.length = 0;
							this.skill_action.result = 'success';
							this.useSkills();
						}
					}
				}
				return;
			}
			if (user.addCardToMonster(card_id, monster_id, part)) {
				socket.broadcast.to(this.id).emit('opponent_drop_card', socket.id);
				io.sockets.in(this.id).emit('add_card_to_monster', {
					card_id: card_id,
					monster_index: monster_id,
					part: part,
					user_id: socket.id
				});
				console.log('User: ' + socket.id + ' put card');
				if (user.table.monsters[monster_id].is_full()) {
					if (!this.skill_action.card || this.skill_action.card.skill != SKILLS.MOCKINGBIRD) {
						this.first_step_legion = deck.cards[card_id - 1].legion;
					}
					this.activateMonster(user, monster_id);
				} else if (this.skill_action.card && this.skill_action.card.skill == SKILLS.MOCKINGBIRD) {
					this.useSkills()
				} else{
					this.first_step_legion = deck.cards[card_id - 1].legion;
					this.nextStep();
				}
			}
		}
	}

	activateMonster(user, monster_id) {
		let monster = user.table.monsters[monster_id];
		if (monster.head.legion == monster.body.legion && monster.body.legion == monster.legs.legion) {
			this.players.forEach((game_player) => {
				if (game_player != user) {
					this.dropLegionCard(game_player, monster.head.legion);
				}
			});
		}
		this.skill_action.part = null;
		this.skill_action.singer_cards = [];
		this.skill_action.monster_id = monster_id;
		this.useSkills();
	}

	useSkills() {
		if(this.skill_action.part == PARTS.LEGS) {
			this.skill_action.result = null;
			this.skill_action.card = null;
			this.skill_action.active = false;
			this.nextStep();
			return;
		}
		let monster_id = this.skill_action.monster_id;
		let parts = [PARTS.HEAD, PARTS.BODY, PARTS.LEGS];
		this.skill_action.part = parts[parts.indexOf(this.skill_action.part) + 1];
		this.skill_action.active = true;
		this.skill_action.result = null;
		this.skill_action.card = null;
		for (let i = parts.indexOf(this.skill_action.part); i < parts.length; i++) {
			this.skill_action.part = parts[i];
			this.skill_action.result = null;
			this.activateSkill(monster_id, parts[i]);
			switch (this.skill_action.result) {
				case 'success':
					break;
				case 'fail':
					this.skill_action.active = false;
					this.nextStep();
					return;
					break;
				case 'wait':
					return;
					break;
			}
		}
		this.skill_action.result = null;
		this.skill_action.card = null;
		this.skill_action.active = false;
		this.nextStep();
	}

	activateSkill(monster_id, part) {
		let user = this.getCurrentUser();
		let socket = user.socket;
		this.skill_action.card = user.table.monsters[monster_id][part];
		switch (user.table.monsters[monster_id][part].skill) {
			case SKILLS.SINGER:
				this.getCard(user);
				this.getCard(user);
				this.skill_action.singer_cards = [user.cards.pop(), user.cards.pop()];
				if(this.skill_action.singer_cards.some((card) => user.table.monsters.some((monster) => monster.allowAddCard(card.id, 'legs') || monster.allowAddCard(card.id, 'body') || monster.allowAddCard(card.id, 'head')))) {
					socket.emit('singer_put_card', {cards: this.skill_action.singer_cards});
					// io.sockets.connected[user.name].emit('singer_put_card', {
					// 	cards: this.skill_action.singer_cards
					// });
					this.skill_action.result = 'wait';
				} else {
					socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
					this.skill_action.singer_cards.forEach((card) => this.trash.push(card));
					this.skill_action.singer_cards.length = 0;
					this.skill_action.result = 'success';
				}
				break;
			case SKILLS.HOWLER:
				let card1 = this.getCard(user),
					card2 = this.getCard(user);
				socket.broadcast.to(this.id).emit('opponent_add_card', socket.id);
				socket.emit('add_card', {card: card1});
				socket.broadcast.to(this.id).emit('opponent_add_card', socket.id);
				socket.emit('add_card', {card: card2});
				this.skill_action.result = 'success';
				break;
			case SKILLS.MOCKINGBIRD:
				if(user.cards.some((card) => user.table.monsters.some((monster) => monster.allowAddCard(card.id, 'legs') || monster.allowAddCard(card.id, 'body') || monster.allowAddCard(card.id, 'head')))) {
					socket.emit('mockingbird_put_card');
					this.skill_action.result = 'wait';
				} else {
					socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
					this.skill_action.result = 'fail';
				}
				break;
			case SKILLS.BUTCHER:
				if(this.players.some((game_player) => (game_player != user) && game_player.table.monsters.some((monster) => !!monster.legs))) {
					socket.emit('butcher_select_monster');
					this.skill_action.result = 'wait';
				} else {
					socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
					this.skill_action.result = 'fail';
				}
				break;
			case SKILLS.SCAVENGER:
				if(this.players.some((game_player) => (game_player != user) && game_player.table.monsters.some((monster) => !!monster.legs && !monster.head))) {
					io.sockets.connected[socket.id].emit('scavenger_select_monster');
					// socket.emit('scavenger_select_monster');
					this.skill_action.result = 'wait';
				} else {
					socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
					this.skill_action.result = 'fail';
				}
				break;
			case SKILLS.DEVOURER:
				if (user.table.monsters.filter((monster, index) => (index != monster_id && !!monster.legs)).length) {
					socket.emit('devourer_select_monster', monster_id);
					this.skill_action.result = 'wait';
				} else {
					socket.emit('skill_have_no_target', user.table.monsters[monster_id][part]);
					this.skill_action.result = 'fail';
				}
				break;
			default:
				this.skill_action.result = 'success';
				break;
		}
	}

	dropLegionCard(game_player, legion) {
		let legion_cards = game_player.cards.filter((card) => {
			return card.legion == legion
		});
		if (legion_cards.length) {
			let drop_index = Math.floor(Math.random() * (legion_cards.length));
			this.dropCard(game_player, legion_cards[drop_index]);
		} else {
			for (let i = 0; i < 2; i++) {
				if (game_player.cards.length) {
					let drop_index = Math.floor(Math.random() * (game_player.cards.length));
					this.dropCard(game_player, game_player.cards[drop_index]);
				}
			}

		}
	}

	dropCard(game_player, dropped_card) {
		game_player.cards.splice(game_player.cards.indexOf(dropped_card), 1);
		this.trash.push(dropped_card);
		game_player.socket.emit('drop_card', {
			card_id: dropped_card.id
		});
		game_player.socket.broadcast.to(this.id).emit('opponent_drop_card', game_player.socket.id);
	}

	getCard(game_player) {
		if (!(game_player instanceof player.Player)) {
			throw new TypeError('cards add only for Players');
		}
		if (!this.deck.length) {
			if (this.trash.length) {
				this.shuffle(this.trash);
				while (this.trash.length) {
					this.deck.push(this.trash.pop())
				}
			} else {
				return -1;
			}
		}
		let card = this.deck.pop();
		game_player.cards.push(card);
		return card;
	}

	nextTurn() {
		this.current_player_index = (this.current_player_index + 1) % this.players_count;
		this.step = 0;
		this.first_step_legion = null;
		io.sockets.in(this.id).emit('next_tourn', this.current_player_index);
	}

	nextStep() {
		this.step = (this.step + 1) % 2;
		if (this.step == 0) {
			this.nextTurn();
		} else {
			this.getCurrentUser().socket.emit('next_step');
		}
	}

	getCurrentUser() {
		return this.players[this.current_player_index];
	}

	leaveUser(socket) {
		for (let i = 0; i < this.players.length; i++) {
			if (this.players[i].socket == socket) {
				this.players.splice(i, 1);
				socket.leave(this.id);
				io.sockets.emit('user_leave_game', {game_id: this.id, user_id: socket.id});
			}
		}
		if (this.game_status != 'prepeareing') {
			this.game_status = 'crashed';
		}
	}

	isUserInGame(socket) {
		let in_game = false;
		this.players.forEach((game_player) => {
			if (game_player.socket == socket) {
				in_game = true;
			}
		});
		return in_game;
	}
}

exports.Game = Game;