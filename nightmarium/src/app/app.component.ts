import { Component } from '@angular/core';
import * as io from 'socket.io-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  game: Game;
  game_list = [];
  dragged_el: any;
  dragged_card: Card;
  socket = null;
  user_id: string;

  constructor() {
    this.socket = io('http://localhost:3000');

    this.socket.on('game_list', (data) => {
      this.game_list = data;
    });

    this.socket.on('user_id', (user_id) => {
      this.user_id = user_id;
    });

    this.socket.on('init_game_user', (data) => {
      let init_hand = [];
      data.cards.forEach((card) => {
        init_hand.push(new Card(card.id, card.legion, card.is_head, card.is_body, card.is_legs, skills[card.skill]));
      });
      this.game.newGame(init_hand);
    });

    this.socket.on('start_game', (data) => {
      data.players.forEach((player) => {
        if (player.user_id == this.user_id) {
          player.is_me = true;
        }
      });
      let cards = data.deck.map((card) => new Card(card.id, card.legion, card.is_head, card.is_body, card.is_legs, skills[card.skill]))
      this.game = new Game(data.players, data.current_player_index, this.socket, cards);
    });

    this.socket.on('game_created', (data) => {
      this.game_list.push({
        game_id: data.game_id,
        max_users: data.max_users,
        max_monsters: data.max_monsters,
        game_status: 'prepeareing',
        players: []
      });
    });

    this.socket.on('game_deleted', (game_id) => {
      for (let i = 0; i < this.game_list.length; i++) {
        if (this.game_list[i].game_id == game_id) {
          this.game_list.splice(i, 1);
          return true;
        }
      }
    });

    this.socket.on('join_game', (data) => {
      this.game_list.forEach((game) => {
        if (game.game_id == data.game_id) {
          game.players.push(data.user_name);
        }
      });
    });

    this.socket.on('next_step', () => {
      this.game.current_step++;
    });

    this.socket.on('next_tourn', (current_player_index) => {
      this.game.current_player_index = current_player_index;
      this.game.current_step = 0;
    });

    this.socket.on('add_card', (data) => {
      let card = data.card;
      //TODO: use deck
      this.game.user.hand.cards.push(new Card(card.id, card.legion, card.is_head, card.is_body, card.is_legs, skills[card.skill]))
    });

    this.socket.on('drop_card', (data) => {
      let card_id = data.card_id;
      //TODO: use deck
      for (let i = 0; i < this.game.user.hand.cards.length; i++) {
        if (this.game.user.hand.cards[i].id == card_id) {
          this.game.user.hand.cards.splice(i, 1);
        }
      }
    });

    this.socket.on('opponent_add_card', (user_id) => {
      this.game.opponents.forEach((player) => {
        (player.name == user_id) && player.cards_count++;
      })
    });

    this.socket.on('opponent_drop_card', (user_id) => {
      this.game.opponents.forEach((player) => {
        (player.name == user_id) && player.cards_count--;
      })
    });

    this.socket.on('you_join_game', (game_id) => {
      this.game_list.forEach((game) => {
        if (game.game_id == game_id) {
          game.players.push(this.user_id);
        }
      });
    });

    this.socket.on('user_leave_game', (data) => {
      this.game_list.forEach((game) => {
        if (game.game_id == data.game_id) {
          let user_index = game.players.indexOf(data.user_id);
          if (user_index != -1) {
            game.players.splice(user_index, 1);
          }
        }
      });
    });

    this.socket.on('devourer_select_monster', (devourer_index) => {
      this.game.skill_action = 'devourer';
      this.game.devourer_index = devourer_index;
    });

    this.socket.on('mockingbird_put_card', () => {
      this.game.skill_action = 'mockingbird';
    });

    this.socket.on('singer_put_card', (data) => {
      this.game.skill_action = 'singer';
      this.game.singer_cards = [];
      data.cards.forEach((card) => {
        this.game.singer_cards.push(this.game.deck.all_cards[card.id - 1])
      })
    });

    this.socket.on('butcher_select_monster', () => {
      this.game.skill_action = 'butcher';
    });

    this.socket.on('scavenger_select_monster', () => {
      this.game.skill_action = 'scavenger';
    });

    this.socket.on('skill_have_no_target', (card) => {
      this.game.singer_cards.length = 0;
      this.game.skill_action = null;
    });

    this.socket.on('user_remove_card_from_monster', (card_id) => {
      this.game.skill_action = null;
      this.game.devourer_index = null;
      this.game.players.forEach((player) => {
        player.table.monsters.forEach((monster) => {
          if (monster.head && monster.head.id == card_id) {
            monster.head = null;
          } else if (monster.body && monster.body.id == card_id) {
            monster.body = null;
          }
          else if (monster.legs && monster.legs.id == card_id) {
            monster.legs = null;
          }
        })
      })
    });

    this.socket.on('add_card_to_monster', (data) => {
      if (data.user_id == this.user_id) {
        if(this.game.skill_action == 'singer') {
          this.game.user.table.monsters[data.monster_index].addCard(
            this.game.deck.all_cards[data.card_id - 1], data.part);
          this.game.singer_cards = [];
        } else {
          this.game.user.addCardToMonster(data.card_id, data.monster_index, data.part);
        }
      } else {
        this.game.opponents.forEach((player: OpponentPlayer) => {
          if (player.name == data.user_id) {
            player.addCardToMonster(this.game.deck.all_cards[data.card_id - 1], data.monster_index, data.part);
          }
        });
      }
    });

  }

  createGame() {
    this.socket.emit('create_game');
  }

  joinGame(game_id) {
    this.socket.emit('join_game', game_id)
  }

  leaveGame(game_id) {
    this.socket.emit('leave_game', game_id)
  }

  startGame(game_id) {
    this.socket.emit('start_game', game_id)
  }

  getGame() {
    for (let i = 0; i < this.game_list.length; i++) {
      for (let j = 0; j < this.game_list[i].players.length; j++) {
        if (this.game_list[i].players[j] == this.user_id) {
          return this.game_list[i];
        }
      }
    }
    return null;
  }

  handleDragStart(e: any, card: Card) {
    this.dragged_card = card;
    this.dragged_el = e.target;
    this.dragged_el.style.opacity = '0.4';
    e.dataTransfer.dropEffect = 'move';
  }

  handleDragOver(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    e.dataTransfer.dropEffect = 'move';
    return false;
  }

  handleDragEnter(e, monster: Monster, part: string) {
    if (monster.allowAddCard(this.dragged_card, part)) {
      e.target.classList.add('card__drag--over');
    }
  }

  handleDragLeave(e) {
    e.target.classList.remove('card__drag--over');
  }

  handleDrop(e, monster: Monster, part: string) {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    e.target.classList.remove('card__drag--over');
    this.game.actionPutCard(this.dragged_card.getId(), monster.index, part);
    this.dragged_card = null;
    return false;
  }

  handleDragEnd(e) {
    this.dragged_el.style.opacity = '1';
    this.dragged_el = null;
    this.dragged_card = null;
  }
}

class Legion {
  static Green = 'green';
  static Blue = 'blue';
  static Red = 'red';
  static Yellow = 'yellow';
}

class Skill {
  name: string;
  description: string;

  constructor(name: string, description: string) {
    this.name = name;
    this.description = description;
  }
}

class Skills {
  singer: Skill;
  howler: Skill;
  mockingbird: Skill;
  butcher: Skill;
  scavenger: Skill;
  devourer: Skill;

  constructor() {
    this.singer = new Skill('singer', 'откройте две карты из колоды. Выложите их перед собой по обычным правилам,' +
      ' если можете сделать это. Те карты, которые вы не можете выложить, сбросьте.');
    this.howler = new Skill('howler', ' возьмите на руку две карты из колоды.');
    this.mockingbird = new Skill('mockingbird', 'выложите одну карту с руки по обычным правилам.');
    this.butcher = new Skill('butcher', 'заберите на руку верхнюю карту любого чужого Существа.');
    this.scavenger = new Skill('scavenger', 'сбросьте любое чужое ущербное (незавершённое) Существо.');
    this.devourer = new Skill('devourer', 'сбросьте верхнюю карту любого своего Существа, кроме этого.');
  }
}

let skills = new Skills();

class Card {
  id: number;
  legion: Legion;
  is_head: boolean;
  is_body: boolean;
  is_legs: boolean;
  skill: Skill | null;

  constructor(id: number, legion: Legion, is_head = false, is_body = false, is_legs = false, skill?: Skill) {
    if (!(is_body || is_head || is_legs)) {
      throw new TypeError('Card mast have one more part of body');
    }
    this.id = id;
    this.legion = legion;
    this.is_head = is_head;
    this.is_body = is_body;
    this.is_legs = is_legs;
    this.skill = skill || null;
  }

  getId() {
    return this.id;
  }

  getLegionClass() {
    return 'card__legion--' + this.legion;
  }

  getSkillClass() {
    return this.skill ? 'card__skill--' + this.skill.name : '';
  }

}

class Deck {
  all_cards: Card[] = [];

  constructor(cards: Card[]) {
    this.all_cards = cards;
    // this.all_cards.push(new Card(1, Legion.Blue, true, false, false));
    // this.all_cards.push(new Card(2, Legion.Blue, true, true, true));
    // this.all_cards.push(new Card(3, Legion.Blue, false, false, true));
    // this.all_cards.push(new Card(4, Legion.Red, true, false, false));
    // this.all_cards.push(new Card(5, Legion.Red, false, true, false));
    // this.all_cards.push(new Card(6, Legion.Red, true, false, true));
    // this.all_cards.push(new Card(7, Legion.Green, true, false, false, skills.singer));
    // this.all_cards.push(new Card(8, Legion.Green, true, true, false, skills.scavenger));
    // this.all_cards.push(new Card(9, Legion.Green, false, false, true, skills.mockingbird));
    // this.all_cards.push(new Card(10, Legion.Yellow, true, false, true, skills.howler));
    // this.all_cards.push(new Card(11, Legion.Yellow, false, true, false, skills.devourer));
    // this.all_cards.push(new Card(12, Legion.Yellow, false, false, true, skills.butcher));
  }
}

class Hand {
  cards: Card[];

  constructor(cards: Card[] = []) {
    this.init(cards);
  }

  getCardsCount() {
    return this.cards.length;
  }

  clear() {
    this.cards.length = 0;
  }

  init(cards: Card[]) {
    if (
      cards.map(function (card: Card) {
        return card.getId();
      })
        .filter(function (item: number, pos: number, self: number[]) {
          return self.indexOf(item) == pos;
        }).length
      != cards.length
    ) {
      throw new TypeError("Card duplicate in hand constructor");
    }
    this.cards = cards.slice();
  }

  hasCard(card: Card): boolean {
    return (this.cards.indexOf(card) != -1);
  }

  getCard(card_id: number): Card {
    for (let i = 0; i < this.cards.length; i++) {
      if (this.cards[i].id == card_id) {
        return this.cards[i];
      }
    }
    return null;
  }

  removeCard(card: Card) {
    let card_index = this.cards.indexOf(card);
    if (card_index != -1) {
      this.cards.splice(card_index, 1);
    }
  }
}

class Monster {
  parts = ['head', 'body', 'legs'];
  head: Card | null;
  body: Card | null;
  legs: Card | null;
  index: number;

  constructor(index) {
    this.index = index;
    this.head = null;
    this.body = null;
    this.legs = null;
  }

  is_full(): boolean {
    return Boolean(this.head && this.body && this.legs);
  }

  allowAddCard(card: Card, part: string): boolean {
    if (!card) {
      return false
    }
    if (!this.legs && part == 'legs' && card.is_legs) {
      return true;
    }
    if (this.legs && !this.body && part == 'body' && card.is_body) {
      return true;
    }
    if (this.legs && this.body && !this.head && part == 'head' && card.is_head) {
      return true;
    }
    return false;
  }

  addCard(card: Card, part: string): boolean {
    if (this.allowAddCard(card, part)) {
      this[part] = card;
      return true;
    }
    return false;
  }

  hasPart(part: string): boolean {
    return !!this.getPart(part);
  }

  getPart(part: string): Card {
    if (part == 'head') {
      return this.head;
    } else if (part == 'body') {
      return this.body;
    } else if (part == 'legs') {
      return this.legs;
    }
    return null;
  }
}

class Table {
  monsters: Monster[] = [];

  constructor(monsters_count: number) {
    this.init(monsters_count)
  }

  clear() {
    this.monsters.length = 0;
  }

  init(monsters_count: number) {
    for (let i = 0; i < monsters_count; i++) {
      this.monsters.push(new Monster(this.monsters.length))
    }
  }

  hasMonster(monster: Monster): boolean {
    return (this.monsters.indexOf(monster) != -1);
  }
}

abstract class Player {
  table: Table;
  index: number;
  name: string;

  constructor(monsters_count: number, index: number, name: string) {
    this.table = new Table(monsters_count);
    this.index = index;
    this.name = name;
  }

  clear() {
    this.table.clear();
  }

  init(init_hand?: Card[]) {
  }

  abstract getCardsCount(): number;
}


class UserPlayer extends Player {
  hand: Hand;

  constructor(monsters_count: number, index: number, name: string) {
    super(monsters_count, index, name);
    this.hand = new Hand();
  }

  clear() {
    this.hand.clear();
  }

  init(init_hand: Card[]): void {
    this.hand.init(init_hand)
  }

  getCardsCount() {
    return this.hand.getCardsCount();
  }

  addCardToMonster(card_id: number, monster_index: number, part: string) {
    let card = this.hand.getCard(card_id);
    if (card && this.table.monsters[monster_index].addCard(card, part)) {
      this.hand.removeCard(card);
    }
  }
}


class OpponentPlayer extends Player {
  cards_count: number = 0;

  constructor(monsters_count: number, index: number, name: string) {
    super(monsters_count, index, name);
    this.cards_count = 5;
  }

  getCardsCount() {
    return this.cards_count;
  }

  clear() {
  }

  init(init_hand?: Card[]) {
  }

  addCardToMonster(card: Card, monster_index: number, part: string) {
    this.table.monsters[monster_index].addCard(card, part)
  }
}

class Game {
  socket: any;
  MAX_MONSTERS_COUNT = 5;
  deck: Deck;
  user: UserPlayer;
  opponents: OpponentPlayer[] = [];
  players: Player[];
  current_player_index: number;
  current_step: number;
  skill_action: string;
  devourer_index: number;
  singer_cards: Card[];

  constructor(players: any[], first_player_index: number, socket: any, deck: Card[]) {
    this.skill_action = null;
    this.devourer_index = null;
    this.singer_cards = [];
    this.deck = new Deck(deck);
    this.socket = socket;
    this.current_player_index = first_player_index;
    this.players = new Array(players.length);
    this.current_step = 0;
    players.forEach((player) => {
      if (player.is_me) {
        this.user = new UserPlayer(this.MAX_MONSTERS_COUNT, player.user_index, player.user_id);
        this.players[player.user_index] = this.user;
      } else {
        this.opponents.push(new OpponentPlayer(this.MAX_MONSTERS_COUNT, player.user_index, player.user_id));
        this.players[player.user_index] = this.opponents.slice(-1).pop();
      }
    });
  }

  newGame(init_hand: Card[]): void {
    this.user.init(init_hand);
  }

  getPlayersCount(): number {
    return this.players.length;
  }

  getCurrentPlayer(): Player {
    if (this.current_player_index == -1) {
      return this.user;
    } else {
      return this.opponents[this.current_player_index];
    }
  }

  isDevourerAvailable(monster:Monster): boolean {
    if(this.skill_action != 'devourer') {
      return false;
    }
    if(this.user.table.monsters[this.devourer_index] == monster) {
      return false;
    }
    return !!monster.legs;
  }

  isButcherAvailable(monster:Monster): boolean {
    if(this.skill_action != 'butcher') {
      return false;
    }
    if(!monster.legs) {
      return false;
    }
    for(let opponent of this.opponents) {
      for(let opponent_monster of opponent.table.monsters) {
        if(opponent_monster == monster) {
          return true;
        }
      }
    }
  }

  isScavengerAvailable(monster:Monster): boolean {
    if(this.skill_action != 'scavenger') {
      return false;
    }
    if(!monster.legs || !!monster.head) {
      return false;
    }
    for(let opponent of this.opponents) {
      for(let opponent_monster of opponent.table.monsters) {
        if(opponent_monster == monster) {
          return true;
        }
      }
    }
  }

  butcher(monster:Monster): void {
    if(this.isButcherAvailable(monster)) {
      for(let opponent of this.opponents) {
        for(let opponent_monster of opponent.table.monsters) {
          if(opponent_monster == monster) {
            this.socket.emit('butcher.select_monster', {
              opponent_id: opponent.name,
              monster_index: opponent.table.monsters.indexOf(monster)
            });
          }
        }
      }
    }
  }

  scavenger(monster:Monster): void {
    if(this.isScavengerAvailable(monster)) {
      for(let opponent of this.opponents) {
        for(let opponent_monster of opponent.table.monsters) {
          if(opponent_monster == monster) {
            this.socket.emit('scavenger.select_monster', {
              opponent_id: opponent.name,
              monster_index: opponent.table.monsters.indexOf(monster)
            });
          }
        }
      }
    }
  }

  devourer(monster:Monster): void {
    if(this.isDevourerAvailable(monster)) {
      this.socket.emit('devourer.select_monster', this.user.table.monsters.indexOf(monster));
    }
  }

  isMyTourn(): boolean {
    return (this.players[this.current_player_index] == this.user)
  }

  actionGetCard(): void {
    if (this.isMyTourn()) {
      this.socket.emit('action_get_card')
    }
  }

  actionPutCard(card_id: number, monster_id: number, part: string): void {
    if (this.isMyTourn()) {
      this.socket.emit('add_card_to_monster', {
        card_id: card_id,
        monster_index: monster_id,
        part: part
      });
    }
  }

  // actionDropAndGet(drop_ids: number[]): void {
  //
  // }
}
