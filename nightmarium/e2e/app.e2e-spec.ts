import { NightmariumPage } from './app.po';

describe('nightmarium App', function() {
  let page: NightmariumPage;

  beforeEach(() => {
    page = new NightmariumPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
