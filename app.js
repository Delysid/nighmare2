let express = require('express');
let path = require('path');
let http = require('http');
let cookie = require('cookie');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(__dirname + '/nightmarium/dist/'));

let games = require('./games');
let deck = require('./deck');

app.get('/*', function (req, res) {
	res.sendFile(path.join(__dirname + '/nightmarium/dist/', 'index.html'))
});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

module.exports = app;

let server = http.createServer(app);

let io = require('socket.io').listen(server);
io.set("origins", "*:*");

global.io = io;

server.listen(3000);




global.PARTS = {
	HEAD: 'head',
	BODY: 'body',
	LEGS: 'legs'
};


global.SKILLS = {
	SINGER: 'singer',
	HOWLER: 'howler',
	MOCKINGBIRD: 'mockingbird',
	BUTCHER: 'butcher',
	SCAVENGER: 'scavenger',
	DEVOURER: 'devourer',
};

global.LEGIONS = {
	BLUE: 'blue',
	RED: 'red',
	YELLOW: 'yellow',
	GREEN: 'green',
};

let all_games = new games.Games();

io.on('connection', function (socket) {
	let cookies = cookie.parse(socket.handshake.headers['cookie']);
	socket.emit('game_list', all_games.getGameList());
	socket.emit('user_id', socket.id);
	console.log('user connected: ' + socket.id + '(cookie: ' + cookies.io + ')');

	socket.on('create_game', function () {
		if (all_games.isUserInGame(socket)) {
			return;
		}
		let game_id = all_games.createGame(socket);
		console.log('User: ' + socket.id + ' create game: ' + game_id);
	});

	socket.on('join_game', function (game_id) {
		let game = all_games.game_list[game_id];
		if (game) {
			game.addPlayer(socket);
		}
		console.log('User: ' + socket.id + ' join game: ' + game.id);
	});

	socket.on('start_game', function (game_id) {
		let game = all_games.game_list[game_id];
		if (game && game.startGame(socket)) {
			console.log('game: ' + game_id + ' started');
		}
	});

	socket.on('leave_game', function (game_id) {
		all_games.leaveUser(socket);
		console.log('User: ' + socket.id + ' leave game: ' + game_id);
	});

	socket.on('action_get_card', function () {
		let game = all_games.getUserGame(socket);
		if (game && !game.skill_action.active) {
			game.actionGetCard(socket);
		}
	});

	socket.on('add_card_to_monster', function (data) {
		let game = all_games.getUserGame(socket);
		if (game && (!game.skill_action.active || game.skill_action.card.skill == SKILLS.MOCKINGBIRD || game.skill_action.card.skill == SKILLS.SINGER)
			&& (game.step==0 || !game.first_step_legion || game.first_step_legion == deck.cards[data.card_id - 1].legion
			|| (game.skill_action.card && (game.skill_action.card.skill == SKILLS.MOCKINGBIRD || game.skill_action.card.skill == SKILLS.SINGER)))) {
			game.actionPutCard(data.card_id, data.monster_index, data.part, socket);
		}
	});

	socket.on('devourer.select_monster', (monster_index) => {
		let game = all_games.getUserGame(socket);
		let user = game.getCurrentUser();
		let monster = user.table.monsters[monster_index];
		if (game && user.socket == socket && game.skill_action.active
			&& game.skill_action.card.skill == SKILLS.DEVOURER
			&& monster.legs
			&& monster[game.skill_action.part] != game.skill_action.card) {
			let removed_card = null;
			if(monster.head) {
				removed_card = monster.head;
				monster.head = null;
			} else if(monster.body) {
				removed_card = monster.body;
				monster.body = null
			} else {
				removed_card = monster.legs;
				monster.legs = null
			}
			game.trash.push(removed_card);
			io.sockets.in(game.id).emit('user_remove_card_from_monster', removed_card.id);
			game.useSkills();
		}
	});

	socket.on('butcher.select_monster', (data) => {
		let game = all_games.getUserGame(socket);
		let user = game.getCurrentUser();
		let opponent = game.players.filter((player) => player.name == data.opponent_id)[0];
		let monster = opponent.table.monsters[data.monster_index];
		if (game && opponent && monster && user.socket == socket && game.skill_action.active
			&& game.skill_action.card.skill == SKILLS.BUTCHER
			&& monster.legs) {
			let removed_card = null;
			if(monster.head) {
				removed_card = monster.head;
				monster.head = null;
			} else if(monster.body) {
				removed_card = monster.body;
				monster.body = null
			} else {
				removed_card = monster.legs;
				monster.legs = null
			}
			user.cards.push(removed_card);
			io.sockets.in(game.id).emit('user_remove_card_from_monster', removed_card.id);
			user.socket.broadcast.to(game.id).emit('opponent_add_card', socket.id);
			user.socket.emit('add_card', {card: removed_card});
			game.useSkills();
		}
	});

	socket.on('scavenger.select_monster', (data) => {
		let game = all_games.getUserGame(socket);
		if (!game) return;
		let user = game.getCurrentUser();
		let opponent = game.players.filter((player) => player.name == data.opponent_id)[0];
		let monster = opponent.table.monsters[data.monster_index];
		if (game && opponent && monster && user.socket == socket && game.skill_action.active
			&& game.skill_action.card.skill == SKILLS.SCAVENGER
			&& monster.legs && !monster.head) {
			let removed_card_legs = monster.legs;
			let removed_card_body = monster.body;
			monster.legs = null;
			monster.body = null;

			if(removed_card_body) {
				game.trash.push(removed_card_body);
				io.sockets.in(game.id).emit('user_remove_card_from_monster', removed_card_body.id);
			}
			game.trash.push(removed_card_legs);
			io.sockets.in(game.id).emit('user_remove_card_from_monster', removed_card_legs.id);
			game.useSkills();
		}
	});

	socket.on('disconnect', function () {
		console.log('Got disconnect: ' + socket.id);
		all_games.leaveUser(socket);
	});
});

console.log('Server Started!');
