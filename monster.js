let deck = require('./deck');

class Monster {
	constructor(index) {
		this.head = null;
		this.body = null;
		this.legs = null;
		this.index = index;
	}

	allowAddCard(card_id, part) {
		let card = deck.cards[card_id - 1];
		if (!card) {
			return false
		}
		if (!this.legs && part == 'legs' && card.is_legs) {
			return true;
		}
		if (this.legs && !this.body && part == 'body' && card.is_body) {
			return true;
		}
		if (this.legs && this.body && !this.head && part == 'head' && card.is_head) {
			return true;
		}
		return false;
	}

	is_full() {
		return Boolean(this.head && this.body && this.legs);
	}
}

exports.Monster = Monster;
