let game = require('./game');
let deck = require('./deck');

class Games {
	constructor() {
		this.game_list = {}
	}

	makeid(len) {
		let text = "";
		let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (let i = 0; i < len; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}

	createGame(socket) {
		let new_id = this.makeid(6);
		while (this.game_list[new_id]) {
			new_id = this.makeid(6);
		}
		this.game_list[new_id] = new game.Game(new_id);
		let new_game = this.game_list[new_id];
		io.sockets.emit('game_created', {
			game_id: new_id,
			max_users: new_game.players_count,
			max_monsters: new_game.monsters_count
		});
		new_game.addPlayer(socket);

		return new_id;
	}

	leaveUser(socket) {
		let user_game = this.getUserGame(socket);
		if (user_game) {
			user_game.leaveUser(socket);
			if (!user_game.players.length) {
				this.deleteGame(user_game.id);
			}
		}
	}

	getGameList() {
		let result = [];
		for (const key in this.game_list) {
			result.push({
				game_id: key,
				max_users: this.game_list[key].players_count,
				max_monsters: this.game_list[key].monsters_count,
				players: this.game_list[key].players.map((player) => player.socket.id),
				game_status: this.game_list[key].status,
			})
		}
		return result;
	}

	getUserGame(socket) {
		for (let key in this.game_list) {
			if (this.game_list[key].isUserInGame(socket)) {
				return this.game_list[key]
			}
		}
		return null;
	}

	deleteGame(game_id) {
		for (let key in this.game_list) {
			if (this.game_list[key].id == game_id) {
				delete this.game_list[key];
				io.sockets.emit('game_deleted', game_id);
				return true
			}
		}
		return false;
	}

	isUserInGame(socket) {
		for (let key in this.game_list) {
			if (this.game_list[key].isUserInGame(socket)) {
				return true
			}
		}
		return false;
	}
}

exports.Games = Games;
