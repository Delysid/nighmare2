class Card {
	constructor(id, legion, is_head = false, is_body = false, is_legs = false, skill = null) {
		if (!(is_body || is_head || is_legs)) {
			throw new TypeError('Card mast have one more part of body');
		}
		this.id = id;
		this.legion = legion;
		this.legion = legion;
		this.is_head = is_head;
		this.is_body = is_body;
		this.is_legs = is_legs;
		this.skill = skill || null;
	}
}

exports.Card = Card;