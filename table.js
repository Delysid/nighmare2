let monster = require('./monster');

class Table {
	constructor(monsters_count) {
		this.monsters = Array.apply(null, Array(monsters_count)).map(function (item, index) {
			return new monster.Monster(index);
		});
	}
}

exports.Table = Table;
